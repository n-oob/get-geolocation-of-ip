#include <stdio.h>
#include <string.h>
#include <curl/curl.h>
#include <json-c/json.h>

int main(int argc, char **argv)
{

	if(argc != 2)
	{
		fprintf(stderr,"\nUsage: %s [ip]\n",argv[0]);
		exit(EXIT_FAILURE);
	}

	CURL *curl;
	int result;
	char url[100] = "http://ipwhois.app/json/";
	strcat(url, argv[1]);
	
	//creting file to store json data
	const char * filename = ".data";
	FILE *fp;
	fp = fopen(filename,"wb");

	curl = curl_easy_init();
	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
	curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1L);

	result=curl_easy_perform(curl);
	if(result == CURLE_OK)
		printf("\n");
	else
		printf("ERROR: %s\n", curl_easy_strerror(result));

	curl_easy_cleanup(curl);
	
	//json object structure to store parsed json
	struct json_object *parsed_json;
	struct json_object *ip;
	struct json_object *continent;
	struct json_object *country;
	struct json_object *country_code;
	struct json_object *country_flag;
	struct json_object *country_capital;
	struct json_object *country_phone;
	struct json_object *country_neighbours;
	struct json_object *region;
	struct json_object *city;
	struct json_object *latitude;
	struct json_object *longitude;
	struct json_object *asn;
	struct json_object *org;
	struct json_object *isp;
	struct json_object *timezone;
	struct json_object *timezone_name;
	struct json_object *timezone_dstOffset;
	struct json_object *timezone_gmtOffset;
	struct json_object *timezone_gmt;
	struct json_object *currency;
	struct json_object *currency_code;
	struct json_object *currency_rates;

	freopen(filename,"rb",fp);
	char buff[1024];
	fread(buff,1024,1,fp);
	fclose(fp);
	remove(filename);
	parsed_json = json_tokener_parse(buff);

	json_object_object_get_ex(parsed_json, "ip", &ip);
	json_object_object_get_ex(parsed_json, "continent", &continent);
	json_object_object_get_ex(parsed_json, "country", &country);
	json_object_object_get_ex(parsed_json, "country_code", &country_code);
	json_object_object_get_ex(parsed_json, "country_flag", &country_flag);
	json_object_object_get_ex(parsed_json, "country_capital", &country_capital);
	json_object_object_get_ex(parsed_json, "country_phone", &country_phone);
	json_object_object_get_ex(parsed_json, "country_neighbours", &country_neighbours);	
	json_object_object_get_ex(parsed_json, "region", &region);
	json_object_object_get_ex(parsed_json, "city", &city);
	json_object_object_get_ex(parsed_json, "latitude", &latitude);
	json_object_object_get_ex(parsed_json, "longitude", &longitude);
	json_object_object_get_ex(parsed_json, "asn", &asn);
	json_object_object_get_ex(parsed_json, "org", &org);
	json_object_object_get_ex(parsed_json, "isp", &isp);
	json_object_object_get_ex(parsed_json, "timezone", &timezone);
	json_object_object_get_ex(parsed_json, "timezone_name", &timezone_name);
	json_object_object_get_ex(parsed_json, "timezone_dstOffset", &timezone_dstOffset);
	json_object_object_get_ex(parsed_json, "timezone_gmtOffset", &timezone_gmtOffset);
	json_object_object_get_ex(parsed_json, "timezone_gmt", &timezone_gmt);
	json_object_object_get_ex(parsed_json, "currency", &currency);
	json_object_object_get_ex(parsed_json, "currency_code", &currency_code);
	json_object_object_get_ex(parsed_json, "currency_rates", &currency_rates);
	
	printf("ip: %s\n",json_object_get_string(ip));
	printf("continent: %s\n",json_object_get_string(continent));
	printf("country: %s\n",json_object_get_string(country));
	printf("country_code: %s\n",json_object_get_string(country_code));
	printf("country_flag: %s\n",json_object_get_string(country_flag));
	printf("country_capital: %s\n",json_object_get_string(country_capital));
	printf("country_phone: %s\n",json_object_get_string(country_phone));
	printf("region: %s\n",json_object_get_string(region));
	printf("city: %s\n",json_object_get_string(city));
	printf("latitude: %s\n",json_object_get_string(latitude));
	printf("longitude: %s\n",json_object_get_string(longitude));
	printf("asn: %s\n",json_object_get_string(asn));
	printf("org: %s\n",json_object_get_string(org));
	printf("isp: %s\n",json_object_get_string(isp));
	printf("timezone: %s\n",json_object_get_string(timezone));
	printf("timezone_name: %s\n",json_object_get_string(timezone_name));
	printf("timezone_dstOffset: %s\n",json_object_get_string(timezone_dstOffset));
	printf("timezone_gmtOffset: %s\n",json_object_get_string(timezone_gmtOffset));
	printf("timezone_gmt: %s\n",json_object_get_string(timezone_gmt));
	printf("currency: %s\n",json_object_get_string(currency));
	printf("currency_code: %s\n",json_object_get_string(currency_code));
	printf("currency_rates: %s\n",json_object_get_string(currency_rates));
	return 0;
}
